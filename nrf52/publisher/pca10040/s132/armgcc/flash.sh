#!/bin/bash
cd ~/git/x/openocd-code
./src/openocd --search tcl  -f interface/stlink-v2.cfg -f target/nrf52.cfg -c init -c "reset init" -c halt -c "nrf5 mass_erase" -c "program $HOME/Downloads/sdk/nRF5_SDK_15.3.0_59ac345/examples/iot/mqtt/lwip/publisher/pca10040/s132/armgcc/_build/nrf52832_xxaa.hex verify" -c "program $HOME/Downloads/sdk/nRF5_SDK_15.3.0_59ac345/components/softdevice/s132/hex/s132_nrf52_6.1.1_softdevice.hex verify" -c "reset" -c "exit"
cd -
