#!/bin/bash
cd ~/git/x/openocd-code
./src/openocd --search tcl  -f interface/stlink-v2.cfg -f target/nrf52.cfg -c init -c "reset init" -c halt -c "reset" -c "exit"
cd -
