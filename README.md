# CO2-Sensor

You will need the nRF52 SDK from nordic-semiconductors.

I used `nRF5_SDK_15.3.0_59ac345`

Copy the directory in nRF52 to `nRF5_SDK_15.3.0_59ac345/examples/iot/mqtt/lwip/publisher/` and build it.

For SWD access the newest OpenOCD-version is needed (for which probably no binary packages exist)